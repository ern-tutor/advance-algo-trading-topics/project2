import datetime

import pandas as pd
import sqlalchemy as db
from dash import Dash, dcc, html, Input, Output
import plotly.express as px

from config import MySQLLoginDetails


app = Dash(__name__)

app.layout = html.Div([
    html.H4('OKX Lending & Borrowing rate (annualized):'),      # HTML Header
    html.P("Select tokens:"),                                   # Dropdown menu title
    dcc.Dropdown(                                               # Dropdown menu
        id="time-series-x-ticker",                              # ID of the menu
        options=["USDT", "BTC", "ETH", "SOL"],
        value="USDT",                                           # Output value
        clearable=False,
    ),
    dcc.Graph(id="time-series-x-time-series-chart"),            # Graph
])


@app.callback(
    Output("time-series-x-time-series-chart", "figure"),
    Input("time-series-x-ticker", "value"))
def display_time_series(ticker='USDT'):
    # Create Database Engine.
    engine = db.create_engine(MySQLLoginDetails.db_url, echo=True)

    with engine.connect() as conn:
        # Read data from table okx_lending_borrowing_rate filtered by the currency.
        df = pd.read_sql("SELECT * FROM okx_lending_borrowing_rate WHERE currency = \"{}\";".format(ticker), conn)
        # Convert receive_unixts to datetime object.
        df['ts'] = df['receive_unixts'].apply(lambda x: datetime.datetime.fromtimestamp(x))
        # Calculate annualized interest rate.
        df['annualized_interest_rate'] = df['rate'] * 24 * 365
        # Line chart.
        fig = px.line(df, x='ts', y='annualized_interest_rate')
        return fig


if __name__ == "__main__":
    app.run_server(debug=False)

import time
import logging
import functools
from datetime import datetime

import schedule
import requests
from sqlalchemy.orm import sessionmaker
import sqlalchemy as db

from models import OKXLendingBorrowingRate
from config import MySQLLoginDetails

logging.basicConfig(
    filename='database_updater.log',
    encoding='utf-8',
    level=logging.DEBUG,
    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s'
)


def print_elapsed_time(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_timestamp = time.time()
        logging.debug('Running job "%s"' % func.__name__)
        result = func(*args, **kwargs)
        logging.debug('Job "%s" completed in %d seconds' % (func.__name__, time.time() - start_timestamp))
        return result

    return wrapper


def catch_exceptions(cancel_on_failure=False):
    def catch_exceptions_decorator(job_func):
        @functools.wraps(job_func)
        def wrapper(*args, **kwargs):
            try:
                return job_func(*args, **kwargs)
            except:
                import traceback
                print(traceback.format_exc())
                if cancel_on_failure:
                    return schedule.CancelJob
        return wrapper
    return catch_exceptions_decorator


@print_elapsed_time
@catch_exceptions(cancel_on_failure=False)
def okx_lending_borrowing_task():
    engine = db.create_engine(MySQLLoginDetails.db_url, echo=True, pool_recycle=28800)
    connection = engine.connect()
    Session = sessionmaker(bind=engine)
    session = Session()

    logging.info('Requesting OKX URL.')
    url = 'https://www.okx.com/api/v5/public/interest-rate-loan-quota'
    req = requests.get(url=url)

    if req.status_code != 200:
        logging.warning(f'Request status code is {req.status_code}.')

    j = req.json()
    data = j['data']
    basic_data = data[0]['basic']
    unix_ts = int(datetime.utcnow().timestamp())

    table_data = []
    for bd in basic_data:
        table_data.append({
            'currency': bd.get('ccy'),
            'quota': float(bd.get('quota')),
            'rate': float(bd.get('rate')),
            'receive_unixts': unix_ts
        })

    logging.info('Inserting to database.')
    insert_query = db.insert(OKXLendingBorrowingRate)
    result_proxy = connection.execute(insert_query, table_data)

    logging.info('Committing Insertion')
    connection.commit()
    logging.info('Committed Insertion')

    connection.close()
    logging.info('Closed Connection.')


schedule.every().hour.at(":58").do(okx_lending_borrowing_task)


if __name__ == '__main__':
    logging.debug('Starts')
    while True:
        schedule.run_pending()
        time.sleep(1)

from sqlalchemy import Column, Integer, VARCHAR, DECIMAL, INT
from base import Base, engine


class OKXLendingBorrowingRate(Base):
    __tablename__ = 'okx_lending_borrowing_rate'

    id = Column(Integer, primary_key=True)
    currency = Column(VARCHAR(255), nullable=False)
    quota = Column(DECIMAL(65, 30))
    rate = Column(DECIMAL(65, 30), nullable=False)
    receive_unixts = Column(INT, nullable=False)

    def __init__(self, currency: str, quota: float, rate: float, receive_unixts: int):
        self.currency = currency
        self.quota = quota
        self.rate = rate
        self.receive_unixts = receive_unixts


if __name__ == '__main__':
    Base.metadata.create_all(engine)

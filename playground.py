import requests
from datetime import datetime
from sqlalchemy.orm import sessionmaker
import sqlalchemy as db

from models import OKXLendingBorrowingRate
from config import MySQLLoginDetails


url = 'https://www.okx.com/api/v5/public/interest-rate-loan-quota'

engine = db.create_engine(MySQLLoginDetails.db_url, echo=True)
connection = engine.connect()

Session = sessionmaker(bind=engine)
session = Session()

req = requests.get(url=url)
j = req.json()
data = j['data']
basic_data = data[0]['basic']

unix_ts = int(datetime.utcnow().timestamp())

table_data = []
for bd in basic_data:
    table_data.append({
        'currency': bd.get('ccy'),
        'quota': float(bd.get('quota')),
        'rate': float(bd.get('rate')),
        'receive_unixts': unix_ts
    })

insert_query = db.insert(OKXLendingBorrowingRate)
result_proxy = connection.execute(insert_query, table_data)

connection.commit()

# Approach 1
# d = []
# for bd in basic_data:
#     d.append(OKXLendingBorrowingRate(
#         currency=bd.get('ccy'),
#         quota=float(bd.get('quota')),
#         rate=float(bd.get('rate')),
#         receive_unixts=unix_ts
#     ))

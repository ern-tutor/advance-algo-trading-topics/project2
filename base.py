from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from config import MySQLLoginDetails

engine = create_engine(MySQLLoginDetails.db_url)
Session = sessionmaker(bind=engine)

Base = declarative_base()

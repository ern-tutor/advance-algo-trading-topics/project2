# Project2


### Installation

---
1. Clone the repository

   - Using SSH
   ```shell script
   git clone ssh://git@gitlab.com:ern-tutor/advance-algo-trading-topics/project2.git
   ``` 
   
   - Using HTTPS with Personal Access Token
   ```shell script
   git clone https://{username}:{personal_acess_token}@gitlab.com/ern-tutor/advance-algo-trading-topics/project2.git
   ```

2. Set up the Virtual Environment

    Ubuntu 20.04 (Debian-based Linux)
    ```shell script
    cd ./project2
    python3.10 -m venv venv/
    source ./venv/bin/activate
    ```
   
    Windows 10
    ```shell script
    cd .\project2
    python -m venv .\venv\
    .\venv\Scripts\activate
    ```

3. Install the dependencies

    ```shell script
    pip install -r requirements.txt
    pip install --upgrade pip
    ```


### Deployment

---
#### Dev Environment
1. Run the application
    ```shell script
    python3.10 main.py
    ```

#### Running via Systemd
1. Move the file to Systemd's system folder.
    ```shell script
    sudo cp ./project2.service /etc/systemd/system/project2.service
    ```
2. Enable and start the service.
    ```shell script
    sudo systemctl daemon-reload
    sudo systemctl enable project2.service
    sudo systemctl start project2.service
    ```
3. Check if the application is running.
    ```shell script
    sudo systemctl status project2.service
    ```
